<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //inicializando variables
        $a=8;
        $b=6;
        //preguntando si a es mayor que b
        if($a>$b){
            //tetxo a mostrar si se cumple la condicion
            print "a es mayor que b<br>";
            //igualando $b a $a
            $b=$a;
        }
        
        if($a>$b):
            print "A es mayor que B<br>";
        endif;
        
        //preguntando si $a es mayor que $b
        if($a>$b){
            print "a es mayor que b<br>";
        }elseif($a==$b){
            //se imprime esto porque son iguales desde que se han igualado arriba
            print "a es igual que b<br>";
        }else
            print "b es mayor que a<br>";
        
        if($a>$b):
            print "A es mayor que B<br>";
            print "...";
        elseif($a==$b):
            //se muestra esto porque se cumple esta condicion
            print "A es igual a B<br>";
            print "!!!";
        else:
            print "B es mayor que A<br>";
        endif;
        ?>
    </body>
</html>
