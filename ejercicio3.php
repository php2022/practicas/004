<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            *{
                margin: 0px;
                padding: 0px;
            }
            span{
                width: 10px;
                height: 50px;
                background-color: #001ee7;
                display: inline-block;
            }
            div{
                height: 50px;
                display: inline-block;
                border: 5px solid #CCC;
                margin: 20px;
            }
        </style>
    </head>
    <body>
        <?php
        //inicializando variable y asignando valores dentro de array
        $datos=[20,40,790,234,890];
        $todo=max($datos);
        //recorriendo array $datos
        foreach($datos as $key=>$value){
            $datos[$key]=(int) (100*$datos[$key]/$todo);
        }
        var_dump($datos);
        //recorriendo array $datos y asignando div con longitud segun el numero que recorre
        foreach($datos as $value){
            echo "<div>";
            for($c=0;$c<$value;$c++){
                echo "<span></span>";
            }
            //mostrando los div en lineas independiantes
            echo "</div>";
            echo "<br>";
        }
        ?>
    </body>
</html>
